package lectureFile;


/*

import org.dhatim.fastexcel.Workbook;
import org.dhatim.fastexcel.Worksheet;
*/

import numericalCalcule.CalculeFormat;
import numericalCalcule.CalculeMethods;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import templatesClasses.Compte;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class MetodsOutInp implements IMetodsOutInp {

    private ArrayList<Compte> arrayListCalc = new ArrayList<Compte>();
    private CalculeFormat calculeFormat =new CalculeFormat();
    public int i=0;
    public int j=0;
    // Default constructor
    public MetodsOutInp(){
        // nothing
    }

    // detecte de lineMax and put error if not continues
    @Override
    public Integer detectLineMax(String file) {


        // position file Excel.xlsx
        String f = new File(file).getAbsolutePath();

        try {

            System.out.println(f);
            FileInputStream fileInputStream = new FileInputStream(f);
            XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
            XSSFSheet ws = wb.getSheet("Sheet1");



            i=0;
            do {
                // get interely line record
                // first record line i=0
                XSSFRow row = ws.getRow(i);

                // array history of empty before entry in function getCell
                if(row==null){
                    System.out.println("empty row");
                    break;
                }
                row.getCell(1);
                row.getCell(0);
                i=i+1;
            } while(true);


        }catch (Exception e){

            System.out.println("exception fouded in subrouitne detectLineMax"+e);
            System.out.println("the file contain some saut on line"+e);

        }
        return i;
    }

    // detecte de Rows Max and put error if not continues
    @Override
    public Integer detectColumnMax(String file){
        System.out.println("in this subroutine is detecte row max" + file);


        return null;
    }

    // eventualli output in file.DAT
    @Override
    public void outputFileDat(String f) {
        System.out.println("in this subroutine is output .DAT");

    }

    /*WRITING FILE EXCEL SUBROUTINE*/
    @Override
    public void writeFileExcel(String fDir) {

        System.out.println("in this subroutine is write Excel Data");

        // position file Excel.xlsx
        String f = new File(fDir).getAbsolutePath();

        try  {
            // detection path file write
            System.out.println(f);

            XSSFWorkbook wb = new XSSFWorkbook();

            XSSFSheet ws = wb.createSheet("sheet3");

            Object empdata[][]={
                    {"EmpID","Empnik"},
                    {101,"Ismail"},
                    {102,"Rama"},
                    {103,"Bharata"},
                    {145,"Mohammed3"},
                    {189,"Krysna"}
            };

            // dim 0
            int limSupi = empdata.length; // lsi : 4
            // dim 1
            int limSupj = empdata[0].length; // lsj : 2
            System.out.println(limSupj + " ____ " + limSupi);
            i=0;

            do{
                System.out.println(i);
                // instance  i-eme rows
                XSSFRow iRow = ws.createRow(i);
                j=0;
                do{
                    // create cell for j-eme columns
                    XSSFCell cell = iRow.createCell(j);

                    System.out.println(cell.getRawValue());

                    Object value = empdata[i][j];

                    // casting

                    if(value instanceof String){
                        cell.setCellValue((String) value);
                    }
                    if(value instanceof Boolean){
                        cell.setCellValue((Boolean) value);
                    }
                    if(value instanceof Integer){
                        cell.setCellValue((Integer)value);
                    }
                    if(value instanceof Double){
                        cell.setCellValue((Double) value);
                    }
                    if(empdata[i][j] instanceof Date){
                        cell.setCellValue((Date) value);
                    }


                    j=j+1;
                }while(j<limSupj);
                i=i+1;
            }while(i<limSupi);

            // note ; after assignement all data, outPut in Data Stream File
            FileOutputStream outputStream = new FileOutputStream(f);
            System.out.println(outputStream);
            wb.write(outputStream);
/*
            Workbook wb = new Workbook(fStream, "Microsoft Excel", "1.0");

            Worksheet ws = wb.newWorksheet("Book1");
            String srt1 = "inputStream";
            ws.value(1,1, LocalDateTime.now());
            ws.value(1,2, srt1);
            ws.value(1,3, 545);
            System.out.println("in this subroutine write File Excel");
            wb.finish();*/
        }catch (Exception e){
            System.out.println("exception fouded in subrouitne writeFileExcel"+e);
        }finally {
            System.out.println("finished subroutine output fileWriter");
        }
    }


    // it need to output a multidimensional array completely of the two files bank and compta


    /*READING FILE EXCEL FUNCTION*/
    @Override
    public ArrayList<Compte> readFileExcel(String f) {
        System.out.println("in this subroutine is read Excel Data");



        try {
            FileInputStream fileInputStream = new FileInputStream(f);
            XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
            XSSFSheet ws =wb.getSheet("Sheet1");



            // this is the limSup for Row
            int limSupi=ws.getLastRowNum();
            // and this is the limSup of Col
            int limSupj=ws.getRow(0).getLastCellNum();
            // getFirstCellNum();
            // starting second reord if is possible
            i=1;
            do{
                // get interely line record
                // first record line i=0
                XSSFRow row = ws.getRow(i);
                // System.out.println(row);

                // initially note: of every iteration need object is another ,
                // with another reference @2324
                Compte compte = new Compte();

                // TODO reconstrure subroutine for decision fields
                j=4;
                do {

                    /* TODO reconstrure subroutine for decision fields
                    output function will be column numberj for recognized label first line ? */

                    if(j==8 || j==9 || j==7 || j==5) {

                        XSSFCell cellExcel = row.getCell(j);
                        // function testing emptyCell

                        System.out.println(calculeFormat.isEmptyCell(cellExcel));

                        // make if else maybe ?
                        // TODO : assignement to Array for calcule in Subroutine
                        // function assignemnt assignemnt array(A[], Type CellExcel.getCellType());

                        System.out.println(j+"_eme column\n");

                        switch(cellExcel.getCellType()) {
                            case STRING:

                                // one only function for assignement
                                // Avoid to recolling same subroutine :
                                if (j==7) {
                                    // System.out.println(cellExcel.getStringCellValue());
                                    compte.libelle = cellExcel.getStringCellValue();
                                }

                                break;

                            case NUMERIC:

                                // System.out.println(cellExcel.getNumericCellValue());
                                // Avoid to recollin same subroutine :
                                // System.out.println(calcule Methods .converterToCent(cellExcel.getNumericCellValue()));

                                double numericOrigin = cellExcel.getNumericCellValue();
                                // legende : (A)j=0;(F)j=5;(I)j=8;(J)j=9
                                if (j==9 || j==8 || j==5) {

                                    // csae datez numerical Excel format 14533
                                    if(j==5){
                                        compte.data1 = calculeFormat.converterToCent(numericOrigin,1);
                                        // case debit credit
                                    }else if(j==9 || j==8){
                                        compte.montant= calculeFormat.converterToCent(numericOrigin,100);
                                        // TODO function boolean for assignement
                                        if(numericOrigin!=0 && j==8){
                                            // debit -> true
                                            compte.sensDC=true;
                                        }else if (numericOrigin!=0 && j==9){
                                            compte.sensDC=false;
                                        }
                                    }
                                }
                                break;
                            case BOOLEAN:
                                // System.out.println(cellExcel.getBooleanCellValue());
                                // Compte compte = new Compte();
                                System.out.println(cellExcel.getBooleanCellValue());
                                break;
                            /* case:
                                System.out.println(cellExcel.getSheet());
                                break;*/
                        }

                    }
                    System.out.println(":");
                    j=j+1;
                }while(j<limSupj);

                // the reference is ripercuted in element of Array list too
                arrayListCalc.add(compte);

                i=i+1;
            }while(i<5);

            // TODO

        }catch (Exception e){
            System.out.println(e+"_:_exception foundent in file read data");
        }

        // initialliy array list for calcule in object arrayList - see if evaluated a multi dimentional array in
        // subroutine calcule numeric
        return arrayListCalc;
    }
}
