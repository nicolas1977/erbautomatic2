package lectureFile;

import templatesClasses.Compte;

import java.util.ArrayList;

public interface IMetodsOutInp {
    void outputFileDat(String f);
    void writeFileExcel(String f);
    ArrayList<Compte> readFileExcel(String f);
    Integer detectLineMax(String file);
    Integer detectColumnMax(String file);

}
