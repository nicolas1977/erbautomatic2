package templatesClasses;

import java.util.Date;

public class Compte
{

    public Compte(){
        // default constructior
    }
    public Compte(int data1, String libelle, Integer montant, Boolean typeBC, Boolean sensDC) {
        this.data1 = data1;
        this.libelle = libelle;
        this.montant = montant;
        this.typeBC = typeBC;
        this.sensDC = sensDC;
    }



    public int data1;
    public String libelle;
    public int montant;
    // type banque -> true
    public Boolean typeBC;
    // debit -> true
    public Boolean sensDC;


}

