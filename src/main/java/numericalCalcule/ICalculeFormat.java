package numericalCalcule;

import org.apache.poi.xssf.usermodel.XSSFCell;

public interface ICalculeFormat {
    public int converterToCent(Double value,int convertTaux);
    public boolean isEmptyCell(XSSFCell cell);
}
