package generalPrompt;

/* Maven dependecies POI :
* https://www.apache.org/dyn/closer.lua/poi/release/bin/poi-bin-5.2.2-20220312.tgz
* */

import lectureFile.MetodsOutInp;
import templatesClasses.Compte;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainClassErb {

    public String fDir;
    public String typeDirFileOutput = "datafiles/file1.xlsx";
    public String typeDirFileInput = "datafiles/file2.xlsx";

    public static void main(String Args[]) throws ParseException {

        System.out.println("this file operationnally");

        MainClassErb mainClassErb = new MainClassErb();
        MetodsOutInp metodsOutInp = new MetodsOutInp();

        // array multidimentionnal structureddyna
        // need to create à new objTemplate in arrayList
        // with differetly referency
        // in every iteration objet template Compte
        /*Compte[] oCompte = new Compte[10];

        System.out.println("first");
        oCompte[0]=new Compte();
        oCompte[1]=new Compte();
        oCompte[2]=new Compte();*/

        // Immpossible  it's de same referency in JVM : Compte objCompte=new Compte();
        // Date date1 = sd. parse(s);
     /*   String data="09/05/2015";
        SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
        oCompte[0].data1= sd.parse(data);
        oCompte[0].libelle="vir r hassan";
        oCompte[0].montant=45;
        oCompte[0].sensDC=true;
        oCompte[0].typeBC=false;*/

/*
        // insert class object records compte bank / compta
        ArrayList<Compte> arrayListCalc = new ArrayList<Compte>();*/


        /*arrayListCalc.add(oCompte[0]);


        oCompte[1].data1= sd.parse(data);
        oCompte[1].libelle="vir r hanifa";
        oCompte[1].montant=550;
        oCompte[1].sensDC=false;
        oCompte[1].typeBC=false;
        arrayListCalc.add(oCompte[1]);


        System.out.println(arrayListCalc);
        System.out.println(arrayListCalc.get(0).libelle);
        System.out.println(arrayListCalc.get(1).libelle);
        */

        // calling control uniform files
        metodsOutInp.detectLineMax(mainClassErb.typeDirFileInput);
        // calling control uniform files
        metodsOutInp.detectColumnMax(mainClassErb.typeDirFileInput);

        // calling subroutine writer
        metodsOutInp.writeFileExcel(mainClassErb.typeDirFileOutput);

        // calling subrouting read
        metodsOutInp.readFileExcel(mainClassErb.typeDirFileInput);

    }
}
